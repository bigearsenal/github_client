//
//  CommitsListViewModel.swift
//  github
//
//  Created by Chung Tran on 31/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm

class CommitsListViewModel: ItemsListViewModel<Commit> {
    var repo: Repository
    
    required init(router: String, predicate: NSPredicate? = nil, repo: Repository) {
        self.repo = repo
        super.init(router: router, predicate: predicate)
    }
    
    override func save(_ newItems: [Commit]) {
        let modifiedNewItems: [Commit] = newItems.map {
            $0.repo = self.repo
            return $0
        }
        
        super.save(modifiedNewItems)
    }
}
