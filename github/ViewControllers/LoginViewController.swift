//
//  ViewController.swift
//  github
//
//  Created by Chung Tran on 26/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit
import WebKit

class LoginViewController: UIViewController, StoryboardInitializableViewController {

    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let authURL = "https://github.com/login/oauth/authorize?client_id=\(GithubAPI.clientID)&scope=repo"
        
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webView.load(urlRequest)
        // Do any additional setup after loading the view, typically from a nib.
    }
}

extension LoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url?.absoluteString,
            let range = url.range(of: "?code=") {
            let code = url[range.upperBound...]
            // login with code
            GithubAPI.default.loginWithCode(String(code))
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)
    }
}

