//
//  CommitCell.swift
//  github
//
//  Created by Chung Tran on 27/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit

class CommitCell: UITableViewCell {
    @IBOutlet weak var hashLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    
    func update(with commit: Commit) {
        hashLabel.text = commit.sha
        messageLabel.text = commit.message
        authorNameLabel.text = commit.authorName
        
        if let date = commit.date {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            dateLabel.text = formatter.string(from: date)
        }
    }
}
