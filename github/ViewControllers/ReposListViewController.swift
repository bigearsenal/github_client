//
//  ReposListViewController.swift
//  github
//
//  Created by Chung Tran on 26/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit
import RxRealmDataSources
import RxSwift
import JGProgressHUD

class ReposListViewController: ItemsListViewController<Repository>, StoryboardInitializableViewController {
    override var router: String! {
        return "/user/repos"
    }
    
    override var dataSource: RxTableViewRealmDataSource<Repository>! {
        return RxTableViewRealmDataSource<Repository>(cellIdentifier: "RepositoryCell", cellType: RepositoryCell.self) { cell, index, repo in
            cell.update(with: repo)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 145
        tableView.rowHeight = UITableView.automaticDimension
        
        title = "My repositories"
        // Do any additional setup after loading the view.
    }
    
    override func bindUI() {
        super.bindUI()
        
        tableView.rx.realmModelSelected(Repository.self)
            .bind { (repo) in
                let vc = CommitsListViewController.fromStoryboard()
                vc.repo = repo
                self.show(vc, sender: nil)
            }
            .disposed(by: bag)
    }

}
