//
//  RepositoryCell.swift
//  github
//
//  Created by Chung Tran on 27/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorAvatar: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var watchersCountLabel: UILabel!
    @IBOutlet weak var folksCountLabel: UILabel!
    
    func update(with repo: Repository) {
        nameLabel.text = repo.name
        descriptionLabel.text = repo.desc
        authorNameLabel.text = repo.authorName
        authorAvatar.sd_setImage(with: URL(string: repo.authorAvatar), completed: nil)
        
        watchersCountLabel.text = "\(repo.watchers)"
        folksCountLabel.text = "\(repo.forks)"
    }
}
