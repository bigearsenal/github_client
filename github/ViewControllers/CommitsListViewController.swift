//
//  CommitsListViewController.swift
//  github
//
//  Created by Chung Tran on 27/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit
import RxRealmDataSources
import RxSwift
import JGProgressHUD

class CommitsListViewController: ItemsListViewController<Commit>, StoryboardInitializableViewController {
    
    override var router: String! {
        return "/repos/\(repo.authorName)/\(repo.name)/commits"
    }
    
    override var predicate: NSPredicate? {
        guard let repo = repo else {return nil}
        return NSPredicate(format: "repo.id == %d", repo.id)
    }
    
    override var dataSource: RxTableViewRealmDataSource<Commit>! {
        return RxTableViewRealmDataSource<Commit>(cellIdentifier: "CommitCell", cellType: CommitCell.self) { cell, index, commit in
            cell.update(with: commit)
        }
    }
    
    var repo: Repository!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 104
        tableView.rowHeight = UITableView.automaticDimension
        
        title = repo.name
        // Do any additional setup after loading the view.
    }
    
    override func createViewModel() {
        viewModel = CommitsListViewModel(router: router, predicate: predicate, repo: repo)
    }

}
