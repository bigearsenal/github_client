//
//  ItemsListViewController.swift
//  github
//
//  Created by Chung Tran on 27/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxRealmDataSources
import RxRealm
import JGProgressHUD

import Unbox

class ItemsListViewController<T>: UIViewController, UITableViewDelegate where T: Object, T: Unboxable {
    var router: String! {
        return nil
    }
    
    var predicate: NSPredicate? {
        return nil
    }
    
    var dataSource: RxTableViewRealmDataSource<T>! {
        return nil
    }
    
    var viewModel: ItemsListViewModel<T>!
    
    @IBOutlet weak var tableView: UITableView!
    internal let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        createViewModel()
        bindUI()
        fetchNext()
    }
    
    func createViewModel() {
        guard let router = router else {
            fatalError("router must be defined")
        }
        viewModel = ItemsListViewModel<T>(router: router, predicate: predicate)
    }
    
    func bindUI() {
        tableView.rx.setDelegate(self)
            .disposed(by: bag)
        
        if dataSource == nil {
            fatalError("need provide dataSource for \(T.self)")
        }
        viewModel.items
            .bind(to: tableView.rx.realmChanges(dataSource))
            .disposed(by: bag)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.isNearBottomEdge(edgeOffset: 20) {fetchNext()}
    }
    
    func fetchNext() {
        self.viewModel.fetchNext()
            .subscribe(
                onError: { [weak self] _ in
                    guard let self = self else {return}
                    let hud = JGProgressHUD(style: .dark)
                    hud.textLabel.text = "Can not retrieve items.\nPlease check your internet connection"
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    hud.isUserInteractionEnabled = true
                    hud.show(in: self.view)
                    hud.dismiss(afterDelay: 3)
            })
            .disposed(by: self.bag)
    }

}
