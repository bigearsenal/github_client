//
//  GithubAPI.swift
//  github
//
//  Created by Chung Tran on 26/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import UIKit
import Alamofire
import RxCocoa
import RxAlamofire
import RxSwift

enum GithubAPIError: Error {
    case invalidToken
}

class GithubAPI {
    
    
    // MARK: - Variables
    private let _endpoint = "https://api.github.com"
    private var _defaultHeaders: HTTPHeaders = [
        "Accept": "application/vnd.github.nightshade-preview+json",
    ]
    static let clientID = "e0621bd0201e9c83f5a5"
    static private let _clientSecret = "7518b080ddf550f449b155505d16b55e7e6e7cd2"
    
    private static let TOKEN_KEY = "USER_TOKEN_KEY"
    private var _token: String? = UserDefaults.standard.string(forKey: GithubAPI.TOKEN_KEY) {
        willSet {
            UserDefaults.standard.set(newValue, forKey: GithubAPI.TOKEN_KEY)
            loggedIn.accept(newValue == nil ? false: true)
        }
    }
    
    // MARK: - Singleton
    public static let `default` = GithubAPI()
    internal required init(){}
    
    // MARK: - Observables
    lazy var loggedIn = BehaviorRelay<Bool>(value: _token == nil ? false: true)
    
    // MARK: - Methods
    open func loginWithCode(_ code: String) {
        let params = [
            "client_id": GithubAPI.clientID,
            "client_secret": GithubAPI._clientSecret,
            "code": code
        ]
        Alamofire.request("https://github.com/login/oauth/access_token", method: .post, parameters: params)
            .validate()
            .responseString { response in
                switch response.result {
                case .success(let value):
                    if let accessTokenBeginIndex = value.range(of: "access_token=")?.upperBound,
                        let accessTokenEndIndex = value.range(of: "&scope")?.lowerBound {
                        
                        let token = value[accessTokenBeginIndex..<accessTokenEndIndex]
                        // Save token
                        self._token = String(token)
                        
                    }
                    // TODO: handle token failed
                    break
                case .failure(let error):
                    // TODO: Handle error
                    print(error)
                    break
                }
        }
    }
    
    func logout() {
        // TODO: Send request to revoke token
        
        // clear token
        _token = nil
    }
    
    // MARK: - Helpers
    func requestWithToken(router: String, method: HTTPMethod, parameters: [String: String]?) -> Observable<(HTTPURLResponse, Any)> {
        guard let token = _token else {return Observable.error(GithubAPIError.invalidToken)}
        var headers = _defaultHeaders
        headers["Authorization"] = "token \(token)"
        return RxAlamofire.requestJSON(method, "\(_endpoint)\(router)", parameters: parameters, headers: headers)
    }
}
