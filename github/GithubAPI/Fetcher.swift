//
//  RepositoryFetcher.swift
//  github
//
//  Created by Chung Tran on 26/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import Foundation
import Alamofire

import RxSwift
import RxCocoa
import RxRealm
import RealmSwift

import Unbox

class Fetcher<T> where T: Object, T: Unboxable {
    enum FetcherError: Error {
        case createRequestFailed, requestFailed
    }
    
    private let per_page = 10
    private var page = 0
    private var reachedTheEnd = false
    private var isLoading = false
    
    private var router: String
    
    init(router: String) {
        self.router = router
    }
    
    let list = BehaviorRelay<[T]>(value: [])
    
    let bag = DisposeBag()
    
    func requestNext() -> Single<[T]> {
        return Single.create { single in
            let disposable = Disposables.create()
            
            // reached the end of the api
            if (self.reachedTheEnd || self.isLoading) {
                return disposable
            }
            
            self.isLoading = true
            
            self.page += 1
            
            GithubAPI.default.requestWithToken(router: "\(self.router)?per_page=\(self.per_page)&page=\(self.page)", method: .get, parameters: nil)
                .subscribe(
                    onNext: { [weak self] (r, json) in
                        let nextValue = (json as! Array).compactMap({ (dict) -> T? in
                            return try? T(unboxer: Unboxer(dictionary: dict))
                        })
                        if (nextValue.count == 0) {
                            // reached the end
                            self?.page -= 1
                            self?.reachedTheEnd = true
                        } else {
                            let nextArray = (self?.list.value ?? []) + nextValue
                            self?.list.accept(nextArray)
                            single(.success(nextValue))
                        }
                        self?.isLoading = false
                    },
                    onError: {[weak self] (error) in
                        self?.page -= 1
                        single(.error(FetcherError.requestFailed))
                        self?.isLoading = false
                    }
                )
                .disposed(by: self.bag)
            
            return disposable
        }
        
    }
}
