//
//  Commit.swift
//  github
//
//  Created by Chung Tran on 27/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox

class Commit: Object, Unboxable {
    // MARK: - Properties
    @objc dynamic var sha: String = ""
    @objc dynamic var message: String = ""
    @objc dynamic var authorName: String = ""
    @objc dynamic var date: Date? = nil
    @objc dynamic var repo: Repository? = nil
    
    // MARK: - Meta
    override static func primaryKey() -> String? {
        return "sha"
    }
    
    // MARK: - Unboxer
    convenience required init(unboxer: Unboxer) throws {
        self.init()
        
        sha = try unboxer.unbox(key: "sha")
        message = try unboxer.unbox(keyPath: "commit.message")
        authorName = try unboxer.unbox(keyPath: "author.login")
        
        let dateFormatter = DateFormatter.iso8601
        date = unboxer.unbox(keyPath: "commit.committer.date", formatter: dateFormatter)
    }
}
