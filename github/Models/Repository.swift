//
//  Repository.swift
//  github
//
//  Created by Chung Tran on 27/03/2019.
//  Copyright © 2019 Chung Tran. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox

class Repository: Object, Unboxable {
    // MARK: - Properties
    @objc dynamic var id: Int64 = 0
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var authorName: String = ""
    @objc dynamic var authorAvatar: String = ""
    @objc dynamic var forks: Int = 0
    @objc dynamic var watchers: Int = 0
    
    // MARK: - Meta
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: - Unboxer
    convenience required init(unboxer: Unboxer) throws {
        self.init()
        
        id = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
        desc = try unboxer.unbox(key: "description")
        authorName = try unboxer.unbox(keyPath: "owner.login")
        authorAvatar = try unboxer.unbox(keyPath: "owner.avatar_url")
        forks = try unboxer.unbox(key: "forks_count")
        watchers = try unboxer.unbox(key: "watchers_count")
    }
}
